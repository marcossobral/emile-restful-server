FROM ruby:2.7

LABEL maintainer="sandroandrade@ifba.edu.br"

RUN apt-get update && apt-get upgrade -y

# Set environment variables.
ENV HOME /var/app

# Define working directory.
COPY . /var/app
RUN cd /var/app; unset BUNDLE_PATH; bundle install

WORKDIR /var/app

EXPOSE 3000

CMD ["/bin/sh", "/var/app/bin/start.sh"]
