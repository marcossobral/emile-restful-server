require 'active_record'

class Question < ActiveRecord::Base
  belongs_to :category
  has_many :question_options, :dependent => :destroy
end

class QuestionOption < ActiveRecord::Base
  belongs_to :question
end

get '/categories/:id/questions' do
  category = Category.find(params[:id]).questions().to_json(:include => [ :question_options ])
end

post '/categories/:id/questions' do
  payload = JSON.parse(request.body.read)
  question = Question.new
  question.name = payload['name']
  question.description = payload['description']
  question.category = Category.find(params[:id])
  question.right_option = payload['right_option']
  question.approved = false
  question.save
  payload['question_options'].each do |option|
    question_option = QuestionOption.new
    question_option.description = option['description']
    question_option.question = question
    question_option.save
  end
  question.to_json(:include => [ :question_options ])
end

put '/categories/:cid/questions/:qid' do
  payload = JSON.parse(request.body.read)
  question = Question.find(params[:qid])
  question.name = payload['name']
  question.description = payload['description']
  question.category = Category.find(params[:cid])
  question.right_option = payload['right_option']
  question.approved = payload['approved']
  question.save
  QuestionOption.destroy_by({ question_id: params[:qid] })
  payload['question_options'].each do |option|
    question_option = QuestionOption.new
    question_option.description = option['description']
    question_option.question = question
    question_option.save
  end
  question.to_json(:include => [ :question_options ])
end

get '/questions/:id' do
  Question.find(params[:id]).to_json(:include => [ :question_options ])
end

get '/pending_questions' do
  Question.where(approved: false).to_json(:include => [ :category, :question_options ])
end

delete '/questions/:id' do
  Question.find(params[:id]).destroy
end
