require 'active_record'

class Category < ActiveRecord::Base
  has_many :questions, :dependent => :destroy
  has_and_belongs_to_many :emilets, join_table: "emilets_categories"
end

get '/categories' do
  Category.all.order(:id).to_json
end

post '/categories' do
  payload = JSON.parse(request.body.read)
  Category.create(name: payload['name']).to_json
end

get '/categories/:id' do
  Category.find(params[:id]).to_json
end

delete '/categories/:id' do
  Category.find(params[:id]).destroy
end
