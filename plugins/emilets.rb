require 'active_record'

class Emilet < ActiveRecord::Base
  has_and_belongs_to_many :categories, join_table: "emilets_categories"
end

get '/emilets' do
  Emilet.all.order(:id).to_json(:include => [ :categories ])
end

post '/emilets' do
  payload = JSON.parse(request.body.read)
  emilet = Emilet.create(name: payload['name'])
  payload['categories'].each do |category|
    emilet.categories << Category.find(category['id'])
  end
  emilet.save
  emilet.to_json(:include => [ :categories ])
end

put '/emilets/:id' do
  payload = JSON.parse(request.body.read)
  emilet = Emilet.find(params[:id])
  emilet.name = payload['name']
  emilet.categories.destroy_all
  payload['categories'].each do |category|
    emilet.categories << Category.find(category['id'])
  end
  emilet.save
  emilet.to_json(:include => [ :categories ])
end

get '/emilets/:id' do
  Emilet.find(params[:id]).to_json
end

delete '/emilets/:id' do
  Emilet.find(params[:id]).destroy
end
