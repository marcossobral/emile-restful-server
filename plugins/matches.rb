require 'active_record'

class Match < ActiveRecord::Base
  belongs_to :emilet
  has_many :match_players, :dependent => :destroy
end

class MatchPlayer < ActiveRecord::Base
  belongs_to :match
end

get '/matches_by_type/:type' do
  if params[:type] == '0'
    Match.where(status: 0..1).to_json(:include => [ :emilet, :match_players ])
  elsif params[:type] == '1'
    Match.where(status: 2).to_json(:include => [ :emilet, :match_players ])
  end
end

get '/matches/:id' do
  Array(Match.find(params[:id])).to_json(:include => [ :match_players ])
end

post '/emilets/:id/matches' do
  payload = JSON.parse(request.body.read)
  match = Match.new
  match.description = payload['description']
  match.emilet = Emilet.find(params[:id])
  match.status = 0
  topic = -1
  loop do
    topic = rand(1..50000)
    break if !Match.find_by({ topic: topic, status: 0..1 })
  end
  match.topic = topic
  match.creator = payload['nickname']
  match.match_players << MatchPlayer.create(player_name: payload['nickname'], approved: true, score: 0)
  match.save
  $publisher.send_string('matches', ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "refresh_matches" }')
  match.to_json(:include => [ :match_players ])
end

put '/matches/:id' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:id]).to_json(:include => [ :match_players ])
  match.status = payload['status']
  match.save
  $publisher.send_string('matches', ZMQ::SNDMORE)
  $publisher.send_string('{ "action": "refresh_matches" }')
  match.to_json(:include => [ :match_players ])
end

post '/matches/:id/players' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:id])
  if match.match_players.length == 4
    '{ "error": "Uma partida não pode ter mais que quatro jogadores!" }'
  else
    match.match_players << MatchPlayer.create(player_name: payload['nickname'], score: 0)
    match.save
    $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
    $publisher.send_string('{ "action": "new_guest", "nickname": "' + payload['nickname'] + '" }')
    match.to_json(:include => [ :match_players ])
  end
end

put '/matches/:mid/players/:pid' do
  payload = JSON.parse(request.body.read)
  match = Match.find(params[:mid])
  player = MatchPlayer.find(params[:pid])
  if payload['nickname'] == match.creator
    if payload['approved'] == true
      player.approved = true
      player.save
      $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
      $publisher.send_string('{ "action": "guest_accepted", "nickname": "' + player.player_name + '" }')
    else
      $publisher.send_string(match.topic.to_s, ZMQ::SNDMORE)
      $publisher.send_string('{ "action": "guest_rejected", "nickname": "' + player.player_name + '" }')
      player.destroy
    end
    match.to_json(:include => [ :match_players ])
  else
    '{ "error": "Somente o criador da partida pode aprovar novos jogadores!" }'
  end
end

